---
title: Careers
---

## Are you ready to take on the challenge?

Are you looking to grow personally and professionally engaging projects that make positive impacts in people's life and planet? 
If you are, then we are in the same page.

Let's walk together on this journey.

> "If you want to walk fast, walk alone.
But if you want to walk far, walk together"
African Proverb.


## Open Positions

### Fullstack Software Engineer (All level)
Location: Seoul Korea

#### What will you do
- Develop features
- Interact with Product Manager and CTO to design and improve the product

#### What You bring
- Degree in computer science, or 2+ years of engineering experience
- Ability to learn in short time
- Proficiency in TypeScript, JavaScript, or Python
- Good communication skills
- Obsession for high quality code

#### Added values
- Hands-on experience with DevSecOps practices
- Knowledgeable in SQL and NoSQL
- Experience deploying in the cloud

#### What we offer
- Flexible working hour
- Possibility of remote working based on productivity
- Sponsored seminars, books


email: joinus@hanaloop.com
