---
title: How we do
---

# How we do

## About HanaLoop Consulting

**HanaLoop Corp.** provides IT consulting services focused on increasing clients’ technological capabilities to harness its potential and maximize business value from the digital world.

<!-- ![HanaLoop](/images/what-we-do/How_we_do_01.JPG) -->
<img src="{{'/images/what-we-do/How_we_do_01.JPG' | url }}" alt="Sustainable Growth" width="500" height="">

## Engagement Process
<!-- ![HanaLoop](/images/what-we-do/How_we_do_04.JPG) -->
<img src="{{'/images/what-we-do/How_we_do_04.JPG' | url }}" alt="Engagement Process" width="800" height="">

## Deliverable Lifecycle: Continuous Delivery
<!-- ![HanaLoop](/images/what-we-do/How_we_do_06.JPG) -->
<img src="{{'/images/what-we-do/How_we_do_06.JPG' | url }}" alt="Engagement Process" width="800" height="">