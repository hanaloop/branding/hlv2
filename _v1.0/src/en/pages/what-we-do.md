---
title: What we do
---

# What we do

**HanaLoop** has two business arms that complement each other. The first arm is the IT consulting services which focuses on clients’ technological capabilities to harness its potential and maximize business value from the digital world.

The second arm is the sustainability platform which helps clients grow ethically in a sustainable way by providing guidance on regulations and insight of the market to create a more responsible and viable value chain.

## Our Value Proposition

### Consulting Services

When you increase the agility of an individual and strengthen the bond among your team, the overall organization performance goes up.
The outcome is faster delivery, higher quality, resulting in happier customers.

We will help you guide your team and organization through the journey of digital transformation. 

* ESG-aligned sustainable transformation
* Focus on organization agility and technical excellence

[How we do](../how-we-do)

### Sustainability

Climate risk is no longer a distant threat. It affects every industry.

Properly managing greenhouse gas not only makes your company more performant and competitive, but also improves branding and employee satisfaction.

* Digitalization of greenhouse gas inventory
* Energy consumption data mining
