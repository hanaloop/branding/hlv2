---
title: Who we are
---

# Who we are

We believe humans are capable of working together to create a better world by combining creativity with ethical practices. 

## Our Mission

Our mission is to lead the transformation of society so that every generation is proud of their legacy, tackling together head-on the most challenging problems.  


## Our Values

* Pursue Excellency
* Create True Value
* Build Long-lasting Trust
* Grow Sustainably
* Thrive Together

## Leadership

<div class="my-5 grid lg:grid-cols-4 grid-cols-2 gap-2">
  {% for teamMember in teamMembers  %}
  <figure class="bg-gray-100 rounded-md my-4 p-4 shadow">
    <img class="w-32 h-32 rounded-full mx-auto" src="{{ teamMember.photo  | url }}" alt="" width="384" height="512">
    <div class="pt-6 space-y-4">
      <figcaption class="font-medium text-center">
        <div class="fond-semibold">
          <a class="no-underline" href="{{ teamMember.profileUrl }}">{{ teamMember.name | safe }}</a>
        </div>
        <div class="text-sm text-primary-dark">
          {{ teamMember.title }}
        </div>
      </figcaption>
      <p class="text-sm">
        {{ teamMember.description }}
      </p>
    </div>
  </figure>
  {% endfor %}
</div>

## Meaning of HanaLoop

**We are Part of One Singe Circle** 

The meaning of the word **'Hana'** comes from Korean Hana (하나) which means One. We believe that everyone and everything is connected and form a one-ness. Every action we do have repercussions and produces consequences that affect others. The word 'Hana' has other meanings in other languages which one way or the other we feel related to.

The word **loop** means circle, ring. We are very conscious that resources are limited, and if we fail to use them wisely, our next generation will be without. Hence, creating a loop is critical, reutilizing the resources again and again in a sustainable way. This applies to all resources: nature, people, as well as emotions, and efforts. 

> From Wikipedia:
> **Hana** as a given name may have any of several origins. It can be a variant transliteration of Hannah, which is the Jewish, French and Christian form, meaning **"grace"** in Hebrew associated with God. It is also a Kurdish name meaning **hope** (هانا), a Persian name meaning **flower** (حَنا), and an Arabic name meaning **happiness and satisfaction** (هَناء). As a Japanese name, it is usually translated as **flower** (花). In Korean, it means the **number one** (하나). In Hawaiian, "Hana" means **"craft"** or **"work"**. In Maori, "Hana" means to **shine, glow, give out love or radiance**.
