---
title: 하나루프 - 기술 역량 향상에 중점을 둔 컨설팅과 지속 가능성 플랫폼
layout: layout/landing.njk

pagination:
  data: collections.posts_en
  size: 10
  reverse: true
  alias: posts

description: 하나루프, 지속가능성 플랫폼, 온실가스 관리, 공급망 탄소관리 플랫폼, 가치사슬망 전반 온실가스 관리 플랫폼, 온실가스 관리 플랫폼, 클라우드 기반 온실가스 관리 플랫폼, 탄소중립 플랫폼  
keywords: ["하나루프", "디지털 전환", "digital transformation", "agile", "애자일", "technology", "기술", "컨설팅", "sustainability", "지속가능성 플랫폼", "공급망 온실가스 관리 플랫폼", "ESG 관리 플랫폼", "친환경 기술", "지속 가능성", "ESG", "온실가스 공급망관리", "온실가스 관리 플랫폼", "탄소중립 플랫폼", "에코루프", "EcoLoop"]

contactUrl: /ko/pages/contact_us
slogan: 자원은 유한하지만<br /> 잠재력은 무한합니다

---

## 하나루프 팀은 지속가능성을 생각하고 실천하며 귀사의 참재력을 최대한 발휘할 수 있도록 도와드리겠습니다 
멈춤 없는 발전의 루프 위에 올라타십시오!

<div class="my-5 grid md:grid-cols-2 grid-cols-1 gap-2">
  <div class="rounded overflow-hidden hover:shadow-lg">
    <a class="no-underline hover:no-underline" href="{{'/ko/pages/what-we-do/' | url }}">
      <img class="full before:bg-black/50" src="{{'/images/dylan-gillis-KdeqA3aTnBY-unsplash.jpg' | url }}">
      <figcaption class="absolute -mt-20 text-white px-4 w-2/5 ">
        <div class="text-xl" style="text-shadow: 1px 1px 5px black;">지속가능한 디지털 전환</div>
      </figcaption>
      <div class="px-6 py-4">
        <div class="text-gray-600">탁월한 IT 컨설팅으로 귀사의 놀라운 도약을 체험하십시오.
        </div>
      </div>
    </a>
  </div>

  <div class="rounded overflow-hidden hover:shadow-lg">
    <a class="no-underline hover:no-underline" href="{{'/ko/pages/what-we-do/' | url }}">
      <img class="full" src="{{'/images/ecoloop-banner.jpg' | url }}">
      <figcaption class="absolute -mt-20 text-white px-4 w-2/5 ">
        <div class="text-xl" style="text-shadow: 1px 1px 5px black;">
        확장성 있는 온실가스 관리 플랫폼</div>
      </figcaption>
      <div class="px-6 py-4">
        <div class="text-gray-600">효율적인 가치사슬망 전반 탄소관리를 통해 기업의 리스크를 관리하고 이미지를 향상시키세요.
        </div>
      </div>
    </a>
  </div>

</div>

## 지구 환경을 지키며 성장하실 수 있습니다!
