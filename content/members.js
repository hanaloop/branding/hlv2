module.exports = {
  "hykim": {
    "name": "김혜연",
    "name_alt": "Hyeyeon Kim",
    "profile_url": "https://www.linkedin.com/in/hyeyeon-kim-49534b1b/",
    "image_url": "/images/who-we-are/hykim-20210704.jpg",
    "title": "co-CEO / 지속가능성 연구원",
    "tagline": "지구환경을 회복시키기 위해 인력 네트워크를 만들려는",
    "description": "온실가스관리기사, LEED Green Associate. 물리, 영상정보처리 전공. 시장 정책 조사"
  },
  "ysahnpark": {
    "name": "안영석",
    "name_alt": "Young-Suk Ahn Park",
    "profile_url": "https://youngsukahn.com/",
    "image_url": "/images/who-we-are/ysahnpark-20130815.jpg",
    "title": "co-CEO / CTO",
    "tagline": "아름다운 지구환경을 딸에게 물려주려는",
    "description": "테크놀로지 리더, 교육가, 소프트웨어 아키텍트. 다양한 규모의 소프트웨어 구축 팀을 리드"
  },
  "hkroh": {
    "name": "노형기",
    "name_alt": "Hyungki Roh",
    "image_url": "/images/who-we-are/kay.jpeg",
    "title": "수석엔지니어",
    "tagline": "기술로 의미있는 일을 하고싶은",
    "description": "다수의 상용 소프트웨어 개발 및 AI 프로젝트 수행. 멀티미디어, 모바일 앱, 웹 플랫폼, 머신 러닝 프로그램 다수 개발"
  }
}
