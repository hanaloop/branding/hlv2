---
title: "Credits"
description: "Credits"
keywords: "image, icons, credits"

publishedAt: "2022-10-06"
---
import SectionBlock from "../components/theme/SectionBlock";

<div className="p-4 [word-break:keep-all]">
The photos and icons used in this site come from the sources listed below:

## Photos
- [Unsplash](https://unsplash.com/)
- [Pexels](https://www.pexels.com/)
- [Pixabay](https://www.pixabay.com/)

## Icons
- [Heroicons](https://heroicons.com/)
- [Bootstrap Icons](https://icons.getbootstrap.com/)
- [Ionic Icons](https://ionic.io/ionicons)
- [css.gg](https://css.gg/app)
- [theforgesmith.com](https://icons.theforgesmith.com/)

</div>


개인정보보호법 및 기업정보 수집/이용/제공 및 활용 동의에 따라 (주)하나루프 정보 신청하시는 분께 수집하는 개인(기업)정보의 항목, 개인(기업)정보의 수집 및 이용목적, 개인(기업)정보의 보유 및 이용기간, 동의 거부권 및 동의 거부 시 불이익에 관한 사항을 안내 드리오니 자세히 읽은 후 동의하여 주시기 바랍니다. (주)하나루프는 서비스 이용 및 회원정보 관리를 위해 필요한 최소한의 개인(기업)정보를 수집합니다. 회원가입 시점에 (주)하나루프가 이용자로부터 수집하는 개인(기업)정보는 안전하게 저장 및 처리됨을 알려드립니다.
