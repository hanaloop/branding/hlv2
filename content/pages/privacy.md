---
---

<div className="p-4">

## 하나루프의 개인정보 처리방침

주식회사 하나루프(이하 “하나루프”라 함)는 “개인정보 보호법” 제30조에 따라 정보주체의 개인정보를 보호하고 이와 관련한 고충을 신속하고 원활하게 처리할 수 있도록 하기 위하여 다음과 같이 개인정보 처리 방침을 수립·공개합니다.


### 제1조(개인정보의 처리목적)

하나루프는 다음의 목적을 위하여 개인정보를 처리합니다. 처리한 개인정보는 다음의 목적이외의 용도로는 사용되지 않으며 이용 목적이 변경되는 경우에는 개인정보 보호법 제18조에 따라 별도의 동의를 받는 등 필요한 조치를 이행할 예정입니다.


#### 가. 홈페이지 회원 가입 및 관리

회원 가입의사 확인, 회원제 서비스 제공에 따른 본인 식별·인증, 회원자격 유지·관리, 서비스 부정이용 방지, 각종 고지·통지, 고충처리 목적으로 개인정보를 처리합니다.


#### 나. 서비스 제공

온실가스 활동자료 및 환경 데이터 처리 서비스 제공, 본인인증, 요금결제·정산, 협업 인프라 제공 등 서비스 제공에 관련한 목적으로 개인정보를 처리합니다.


#### 다. 고충처리

고충의뢰인의 신원 확인, 고충사항 확인, 사실조사를 위한 연락·통지, 처리결과 통보의 목적으로 개인정보를 처리합니다.


### 라. 하나루프의 개인정보 처리업무

[개인정보 처리업무에 따른 처리목적]으로 개인정보를 처리합니다.


### 제2조(개인정보의 처리 및 보유기간) 

하나루프는 법령에 따른 개인정보 보유·이용기간 또는 정보주체로부터 개인정보를 수집시에 동의받은 개인정보 보유·이용기간 내에서 개인정보를 처리·보유합니다.

각각의 개인정보 처리 및 보유 기간은 다음과 같습니다.


1. 홈페이지 회원 가입 및 관리 : 사업자/단체 홈페이지 탈퇴 시까지. 다만, 다음의 사유에 해당하는 경우에는 해당 사유 종료 시까지
    1. 관계 법령 위반에 따른 수사조사 등이 진행 중인 경우에는 해당 수사조사 종료 시까지
    2. 홈페이지 이용에 따른 채권·채무관계 잔존 시에는 해당 채권·채무관계 정산 시까지
    3. [예외 사유] 시에는 [보유기간] 까지
2. 재화 또는 서비스 제공 : 재화·서비스 공급완료 및 요금결제·정산 완료시까지. 다만, 다음의 사유에 해당하는 경우에는 해당 기간 종료 시까지
1. “전자상거래 등에서의 소비자 보호에 관한 법률”에 따른 표시·광고, 계약내용 및 이행 등 거래에 관한 기록
* 표시·광고에 관한 기록 : 6개월 
* 계약 또는 청약철회, 대금결제, 재화 등의 공급기록 : 5년
* 소비자 불만 또는 분쟁처리에 관한 기록 : 3년
2. “통신비밀보호법”에 따른 통신사실확인자료 보관
* 가입자 전기통신일시, 개시·종료시간, 상대방 가입자번호, 사용도수, 발신기지국 위치추적자료 : 1년
* 컴퓨터 통신, 인터넷 로그기록자료, 접속지 추적자료 : 3개월


### 제3조 (개인정보의 제3자 제공) (해당되는 경우에만 정함) 

개인정보보호위원회는 정보주체의 동의, 법률의 특별한 규정 등 개인정보 보호법 제17조 및 제18조에 해당하는 경우에만 개인정보를 제3자에게 제공합니다.

개인정보보호위원회는 민원 신청인이 공공기관에 대하여 신청한 개인정보 열람, 정정·삭제, 처리정지민원을 처리하기 위하여 민원 신청인의 개인정보를 개인정보파일 보유기관에게 이송(제공)하고 있으며, [민원사무 처리에 관한 법률]에서 정하는 기간 동안 해당 기관에서 보유 및 이용합니다.

- 이송(제공)하는 개인정보 항목 : 신청인 성명, 생년월일, 전화번호, 주소


### 제4조 (개인정보처리의 위탁)

하나루프는 원활한 개인정보 업무처리를 위하여 다음과 같이 개인정보 처리업무를 위탁하고 있습니다.


* 가. 위탁처리기관
* 나. 위탁처리 수행업체

(본인인증서비스)

a. Auth0.com 사용


### 제5조 (정보주체와 법정대리인의 권리·의무 및 행사방법)



1. 정보주체(만 14세 미만인 경우에는 법정대리인을 말함)는 언제든지 하나루프가 보유하고 있는 개인정보에 대하여 개인정보 열람․정정․삭제․처리정지 요구 등의 권리를 행사할 수 있습니다.
2. 제1항에 따른 권리 행사는 하나루프에 대해 개인정보보호법 시행령 제41조제1항에 따라 서면, 전자우편 등을 통하여 하실 수 있으며, 하나루프는 이에 대해 지체없이 조치하겠습니다.
3. 제1항에 따른 권리 행사는 정보주체의 법정대리인이나 위임을 받은 자 등 대리인을 통하여 하실 수 있습니다. 이 경우 “개인정보 처리 방법에 관한 고시(제2020-7호)” 별지 제11호 서식에 따른 위임장을 제출하셔야 합니다.
4. 개인정보 열람 및 처리정지 요구는 개인정보보호법 제35조 제4항, 제37조 제2항에 의하여 정보주체의 권리가 제한 될 수 있습니다.
5. 개인정보의 정정 및 삭제 요구는 다른 법령에서 그 개인정보가 수집 대상으로 명시되어 있는 경우에는 그 삭제를 요구할 수 없습니다.
6. 하나루프는 정보주체 권리에 따른 열람의 요구, 정정·삭제의 요구, 처리정지의 요구 시 열람 등 요구를 한 자가 본인이거나 정당한 대리인인지를 확인합니다.


### 제6조 (처리하는 개인정보 항목) 

하나루프는 다음의 개인정보 항목을 처리하고 있습니다.



1. 홈페이지 회원 가입 및 관리
* 필수항목 : 성명, 아이디, 비밀번호, 전화번호, 이메일주소
* 선택항목 : 주소, 관심분야
2. 서비스 제공
* 필수항목 : 성명, 아이디, 비밀번호, 전화번호, 이메일주소, 신용카드번호, 은행계좌정보
* 선택항목 : 주소, 관심분야
3. [개인정보 처리업무]
* 필수항목 : [처리항목]
* 선택항목 : [처리항목]
4. [개인정보 처리업무] (필수항목만 수집하는 경우)
* [처리항목], [처리항목]
5. 인터넷 서비스 이용 과정에서 아래 개인정보 항목이 자동으로 생성되어 수집될 수 있습니다.
* IP 주소, 쿠키, MAC 주소, 서비스 이용기록, 방문기록 


### 제7조 (개인정보 파기 절차 및 파기방법)

하나루프는 원칙적으로 개인정보 처리목적이 달성된 경우에는 지체없이 해당 개인정보를 파기합니다. 다만, 다른 법률에 따라 보존하여야하는 경우에는 그러하지 않습니다. 파기의 절차, 기한 및 방법은 다음과 같습니다.


#### 가. 파기절차

불필요한 개인정보 및 개인정보파일은 개인정보책임자의 책임 하에 내부방침 절차에 따라 다음과 같이 처리하고 있습니다.



* 개인정보의 파기
    * 보유기간이 경과한 개인정보는 종료일로부터 지체 없이 파기합니다.
* 개인정보파일의 파기
    * 개인정보파일의 처리 목적 달성, 해당 서비스의 폐지, 사업의 종료 등 그 개인정보파일이 불필요하게 되었을 때에는 개인정보의 처리가 불필요한 것으로 인정되는 날로부터 지체 없이 그 개인정보파일을 파기합니다.


#### 나. 파기방법



1. 전자적 형태의 정보는 기록을 재생할 수 없는 기술적 방법을 사용합니다.
2. 종이에 출력된 개인정보는 분쇄기로 분쇄하거나 소각을 통하여 파기합니다.


### 제8조(개인정보의 안전성 확보조치) 

하나루프는 개인정보의 안전성 확보를 위해 다음과 같은 조치를 취하고 있습니다.



1. 관리적 조치 : 내부관리계획 수립·시행, 정기적 직원 교육 등
2. 기술적 조치 : 개인정보시스템 등의 접근권한 관리, 접근통제시스템 설치, 고유식별정보 등의 암호화, 보안프로그램 설치
3. 물리적 조치 : 전산실, 자료보관실 등의 접근통제


### 제9조 (개인정보 자동 수집 장치의 설치·운영 및 그 거부)



1. 하나루프는 이용자에게 개별적인 맞춤서비스를 제공하기 위해 이용정보를 저장하고 수시로 불러오는 '쿠키(cookie)'를 사용합니다.
2. 쿠키는 웹사이트를 운영하는데 이용되는 서버(http)가 이용자의 컴퓨터 브라우저에게 보내는 소량의 정보이며 이용자들의 PC 컴퓨터내의 하드디스크에 저장되기도 합니다.
    1. 쿠키의 사용목적 : 이용자가 방문한 각 서비스와 웹사이트들에 대한 방문 및 이용형태, 인기 검색어, 보안접속 여부, 등을 파악하여 이용자에게 최적화된 정보 제공을 위해 사용됩니다.
    2. 쿠키의 설치·운영 및 거부: 브라우저 옵션 설정을 통해 쿠키 허용, 쿠키 차단 등의 설정을 할 수 있습니다.
        * Internet Explorer : 웹브라우저 우측 상단의 도구 메뉴 > 인터넷 옵션 > 개인정보 > 설정 > 고급
        * Edge: 웹브라우저 우측 상단의 설정 메뉴 > 쿠키 및 사이트 권한 > 쿠키 및 사이트 데이터 관리 및 삭제
        * Chrome: 웹브라우저 우측 상단의 설정 메뉴 > 개인정보 및 보안 > 쿠키 및 기타 사이트 데이터
    3. 쿠키 저장을 거부 또는 차단할 경우 서비스 이용에 어려움이 발생할 수 있습니다.


### 제10조(개인정보 보호책임자) 

하나루프는 개인정보 처리에 관한 업무를 총괄해서 책임지고, 개인정보 처리와 관련한 정보주체의 불만처리 및 피해구제 등을 위하여 아래와 같이 개인정보보호 책임자를 지정하고 있습니다.

▶ 개인정보 보호책임자

성명 : 김혜연

직책 : 대표

연락처 : [info@hanaloop.com](mailto:info@hanaloop.com)

 정보주체께서는 하나루프의 서비스(또는 사업)을 이용하시면서 발생한 모든 개인정보 보호 관련 문의, 불만처리, 피해구제 등에 관한 사항을 개인정보 보호책임자 및 담당부서로 문의하실 수 있습니다. 하나루프는 정보주체의 문의에 대해 지체없이 답변 및 처리해드릴 것입니다.


### 제12조(국내대리인의 지정) 

정보주체는 “개인정보 보호”법 제39조의 11에 따라 지정된 하나루프의 국내대리인에게 개인정보 관련 고충처리 등의 업무를 위하여 연락을 취할 수 있습니다. 하나루프는 정보주체의 개인정보 관련 고충처리 등 개인정보 보호책임자의 업무 등을 신속하게 처리할 수 있도록 노력하겠습니다.

▶ 하나루프는 『개인정보 보호법』 제39조의11에 따라 국내대리인을 지정하였습니다.



* 국내대리인의 성명: 김혜연
* 국내대리인의 전자우편주소: info@hanaloop.com


### 제13조(추가적인 이용⋅제공 판단기준) 

하나루프는 『개인정보 보호법』 제15조제3항 및 제17조제4항에 따라  『개인정보 보호법 시행령』 제14조의2에 따른 사항을 고려하여 정보주체의 동의 없이 개인정보를 추가적으로 이용⋅제공할 수 있습니다.

이에 따라 하나루프가 정보주체의 동의 없이 추가적인 이용⋅제공을 하기 위해서 다음과 같은 사항을 고려하였습니다.



* 개인정보를 추가적으로 이용⋅제공하려는 목적이 당초 수집 목적과 관련성이 있는지 여부
* 개인정보를 수집한 정황 또는 처리 관행에 비추어 볼 때 추가적인 이용⋅제공에 대한 예측 가능성이 있는지 여부
* 개인정보의 추가적인 이용⋅제공이 정보주체의 이익을 부당하게 침해하는지 여부
* 가명처리 또는 암호화 등 안전성 확보에 필요한 조치를 하였는지 여부


### 제14조(개인정보 열람청구) 

정보주체는 『개인정보 보호법』 제35조에 따른 개인정보의 열람 청구를 아래의 부서에 할 수 있습니다. 하나루프는 정보주체의 개인정보 열람청구가 신속하게 처리되도록 노력하겠습니다.


    ▶ 개인정보 열람청구 접수·처리 부서
    부서명 : 개발지원부
    담당자 : 김혜연
    연락처 : [info@hanaloop.com](mailto:info@hanaloop.com)

### 
    제15조 (권익침해에 대한 구제방법)

정보주체는 개인정보침해로 인한 구제를 받기 위하여 개인정보분쟁조정위원회, 한국인터넷진흥원 개인정보침해신고센터 등에 분쟁해결이나 상담 등을 신청할 수 있습니다. 이 밖에 기타 개인정보침해의 신고, 상담에 대하여는 아래의 기관에 문의하시기 바랍니다.


* 개인정보 침해신고센터 (한국인터넷진흥원 운영)
    * 소관업무 : 개인정보 침해사실 신고, 상담 신청
    * 홈페이지 : privacy.kisa.or.kr
    * 전화 : (국번없이) 118
    * 주소 : 전라남도 나주시 진흥길 9 한국인터넷진흥원
* 개인정보 분쟁조정위원회
    * 소관업무 : 개인정보 분쟁조정신청, 집단분쟁조정 (민사적 해결)
    * 홈페이지 : www.kopico.go.kr
    * 전화 : 1833-6972
    * 주소 : 서울특별시 종로구 세종대로 209 정부서울청사 12층
* 대검찰청 사이버수사과:
    * (국번없이) 1301, privacy@spo.go.kr (www.spo.go.kr)
* 경찰청 사이버수사국
    * (국번없이) 182 (ecrm.cyber.go.kr)

또한, 개인정보의 열람, 정정·삭제, 처리정지 등에 대한 정보주체자의 요구에 대하여 공공기관의 장이 행한 처분 또는 부작위로 인하여 권리 또는 이익을 침해 받은 자는 행정심판법이 정하는 바에 따라 행정심판을 청구할 수 있습니다.


    ※ 중앙행정심판위원회: (국번없이) 110 (www.simpan.go.kr)


### 제16조 (개인정보 처리방침의 변경에 관한 사항)

하나루프의 개인정보 처리방침은 2022년 9월 1일부터 시행됩니다.

개인정보처리방침 버전번호 : v0.8

개인정보처리방침 변경일자 : 2022년 9월 1일

개인정보처리방침 시행일자 : 2022년 9월 1일

</div>
