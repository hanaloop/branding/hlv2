import { DisplayItem } from "../../libs/types"

const benefits: DisplayItem[] = [
  {
    title: "데이터 연동, 자동화 및 분석",
    description: <>
        <ul>
        <li>번거로운 수작업 데이터 수집 줄임 </li>
        <li>전사 배출 현황 및 추세 분석 제공</li>
        <li>규제 변화에 대응 용이</li>
        <li>안전하고 확장 가능</li>
        </ul>
      </>
  },
  {
    title: "가치사슬망 스코프 3 산정",
    description: <>
      <ul>
      <li>가치사슬 배출량 산정 </li>
      <li>맞춤형 보고서</li>
      <li>다국어 지원</li>
      </ul>
      </>
  },
  {
    title: "중소기업에게 부담 없는 가격",
    description: <>
      <ul>
      <li>고비용 투자 없이 배출관리 가능</li>
      <li>기업 규모에 따른 가격 체계</li>
      <li>시스템 전담인력 필요 없음</li>
      <li>별도 라이선스 비용 없음</li>
      </ul>
    </>
  },
  {
    title: "OpenAPI - 플랫폼",
    description: <>
      <ul>
      <li>창의적인 솔루션 구축시간 단축</li>
      <li>신사업 모델 창출 가능</li>
      <li>온실가스 관리문화 형성 </li>
      </ul>
    </>
  }
]

module.exports = {
  // areas,
  benefits
}
